import random
#from random import randint
board = []
board.append([" ", "1", "2", "3", "4", "5"])
board.append(["1", "O", "O", "O", "O", "O"])
board.append(["2", "O", "O", "O", "O", "O"])
board.append(["3", "O", "O", "O", "O", "O"])
board.append(["4", "O", "O", "O", "O", "O"])
board.append(["5", "O", "O", "O", "O", "O"])

#board.append(["O"] * 5)
#for x in range(0, 5):
  #board.append([str(x) + "O" * 5])

def print_board(board):
  for row in board:
    print " ".join(row)

print_board(board)

ship_row = random.randint(1, 6)
ship_col = random.randint(1, 6)
print ship_row
print ship_col
def close():
  if guess_row == ship_row or guess_row == ship_row + 1 or guess_row == ship_row - 1 and \
  guess_col == ship_col or guess_col == ship_col + 1 or guess_col == ship_col - 1:
    print '...Computer sweats...'
  else:
    print "Computer is confident"
# Everything from here on should be in your for loop
# don't forget to properly indent!
for turn in range(4):
  print "Turn", turn + 1
  guess_row = int(raw_input("Guess Row: "))
  guess_col = int(raw_input("Guess Col: "))
  
  if guess_row == ship_row and guess_col == ship_col:
    print "Congratulations! You sank my battleship!"
    break
  else:
    if guess_row not in range(1, 6) or \
      guess_col not in range(1, 6):
      print "Oops, that's not even in the ocean."
      close()
    elif board[guess_row][guess_col] == "X":
      print( "You guessed that one already." )
    else:
      print "You missed my battleship!"
      board[guess_row][guess_col] = "X"
      close()
    if (turn == 3):
      print "Game Over!"
    print_board(board)
